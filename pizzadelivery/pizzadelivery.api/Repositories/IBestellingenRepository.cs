﻿using pizzadelivery.models.Bestellingen;
using System;
using System.Threading.Tasks;

namespace pizzadelivery.api.Repositories
{
    public interface IBestellingenRepository
    {
        Task<GetBestellingenModel> GetBestellingen();
        Task<GetBestellingenModel> GetBestellingen(Guid id);
        Task<GetBestellingenModel> PostBestelling(PostBestellingModel postBestellingModel);
        Task PutBestelling(Guid id, PutBestellingModel putBestellingModel);
        Task DeleteBestelling(Guid id);
    }
}