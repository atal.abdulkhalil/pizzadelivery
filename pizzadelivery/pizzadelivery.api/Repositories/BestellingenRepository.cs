﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using pizzadelivery.api.Entities;
using pizzadelivery.models.Bestellingen;
using pizzadelivery.models.StandaardPizza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace pizzadelivery.api.Repositories
{
    public class BestellingenRepository : IBestellingenRepository
    {
        private readonly PizzaDeliveryContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ClaimsPrincipal _user;

        public BestellingenRepository(
            PizzaDeliveryContext context,
            IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _user = _httpContextAccessor.HttpContext.User;
        }

        public async Task<GetBestellingenModel> GetBestellingen()
        {
            List<GetBestellingModel> bestelling = await _context.Bestellingen
                .Include(x => x.Klant)
                .Include(x => x.Bereider)
                .Include(x => x.Vervoerder)
                .Include(x => x.Pizzas)
                    .ThenInclude(x => x.StandaardPizza)
                .Select(x => new GetBestellingModel
                {
                    Id = x.Id,
                    TijdstipBestelling = (DateTime)x.TijdstipBestelling,
                    TijdstipLevering = (DateTime)x.TijdstipLevering == null ? default(DateTime) : (DateTime)x.TijdstipLevering,
                    KlantId = x.Klant.Id,
                    Klant = x.Klant.Voornaam + " " + x.Klant.Familienaam,
                    BereiderId = x.Bereider.Id == null ? Guid.Empty : (Guid)x.BereiderId,
                    Bereider = x.Bereider.Voornaam + " " + x.Bereider.Familienaam,
                    VervoerderId = x.Vervoerder.Id == null ? Guid.Empty : (Guid)x.VervoerderId,
                    Vervoerder = x.Vervoerder.Voornaam + " " + x.Vervoerder.Familienaam,
                    VoertuigId = x.Voertuig.Id == null ? Guid.Empty : (Guid)x.VoertuigId,
                    Pizzas = x.Pizzas.Select(x => new GetStandaardPizzaModel
                    {
                        Naam = x.StandaardPizza.Naam,
                        EenheidPrijs = x.Formaat.Equals("klein") ? x.StandaardPizza.PrijsKlein : x.Formaat.Equals("medium") ? x.StandaardPizza.PrijsMedium : x.StandaardPizza.PrijsGroot,
                        TotaalPrijs = x.Formaat.Equals("klein") ? x.StandaardPizza.PrijsKlein * x.Aantal : x.Formaat.Equals("medium") ? x.StandaardPizza.PrijsMedium * x.Aantal : x.StandaardPizza.PrijsGroot * x.Aantal,
                        Aantal = x.Aantal,
                        Formaat = x.Formaat,
                    }).ToList(),
                    TotaalPrijs = x.Pizzas.Select(x => (x.Formaat.Equals("klein") ? x.StandaardPizza.PrijsKlein :
                                                       x.Formaat.Equals("medium") ? x.StandaardPizza.PrijsMedium :
                                                       x.StandaardPizza.PrijsGroot) * x.Aantal).Sum()
                })
                .AsNoTracking()
                .ToListAsync();
            GetBestellingenModel bestellingen = new GetBestellingenModel
            {
                Bestelling = bestelling
            };


            List<GetBestellingModel> kbestelling = new List<GetBestellingModel>();

            GetBestellingenModel kbestellingen = new GetBestellingenModel
            {
                Bestelling = kbestelling
            };
            if (_user.Claims.Where(x => x.Type.Contains("role")).Count() == 1 &&
                _user.IsInRole("Klant"))
            {
                for (int i = 0; i < bestellingen.Bestelling.Count(); i++)
                {
                    if (_user.Claims.Where(x => x.Type.Contains("role")).Count() == 1 &&
                    _user.IsInRole("Klant") &&
                    _user.Identity.Name == bestelling[i].KlantId.ToString())
                    {
                        kbestelling.Add(bestellingen.Bestelling[i]);
                    }
                }
                return kbestellingen;
            }

            return bestellingen;
        }



        public async Task<GetBestellingenModel> GetBestellingen(Guid id)
        {
            List<GetBestellingModel> bestelling = await _context.Bestellingen
                .Include(x => x.Klant)
                .Include(x => x.Bereider)
                .Include(x => x.Vervoerder)
                .Include(x => x.Pizzas)
                    .ThenInclude(x => x.StandaardPizza)
                .Where(x => x.Id == id)
                .Select(x => new GetBestellingModel
                {
                    Id = x.Id,
                    TijdstipBestelling = (DateTime)x.TijdstipBestelling,
                    TijdstipLevering = (DateTime)x.TijdstipLevering == null ? default(DateTime) : (DateTime)x.TijdstipLevering,
                    KlantId = x.Klant.Id,
                    Klant = x.Klant.Voornaam + " " + x.Klant.Familienaam,
                    BereiderId = x.Bereider.Id == null ? Guid.Empty : (Guid)x.BereiderId,
                    Bereider = x.Bereider.Voornaam + " " + x.Bereider.Familienaam,
                    VervoerderId = x.Vervoerder.Id == null ? Guid.Empty : (Guid)x.VervoerderId,
                    Vervoerder = x.Vervoerder.Voornaam + " " + x.Vervoerder.Familienaam,
                    VoertuigId = x.Voertuig.Id == null ? Guid.Empty : (Guid)x.VoertuigId,
                    Pizzas = x.Pizzas.Select(x => new GetStandaardPizzaModel
                    {
                        Naam = x.StandaardPizza.Naam,
                        EenheidPrijs = x.Formaat.Equals("klein") ? x.StandaardPizza.PrijsKlein : x.Formaat.Equals("medium") ? x.StandaardPizza.PrijsMedium : x.StandaardPizza.PrijsGroot,
                        TotaalPrijs = x.Formaat.Equals("klein") ? x.StandaardPizza.PrijsKlein * x.Aantal : x.Formaat.Equals("medium") ? x.StandaardPizza.PrijsMedium * x.Aantal : x.StandaardPizza.PrijsGroot * x.Aantal,
                        Aantal = x.Aantal,
                        Formaat = x.Formaat,
                    }).ToList(),
                    TotaalPrijs = x.Pizzas.Select(x => (x.Formaat.Equals("klein") ? x.StandaardPizza.PrijsKlein :
                                                      x.Formaat.Equals("medium") ? x.StandaardPizza.PrijsMedium :
                                                      x.StandaardPizza.PrijsGroot) * x.Aantal).Sum()
                })
                .AsNoTracking()
                .ToListAsync();
            GetBestellingenModel bestellingen = new GetBestellingenModel
            {
                Bestelling = bestelling
            };
            if (bestellingen.Bestelling.Count() == 0)
            {
                throw new Exception("404");
            }
            if (_user.Claims.Where(x => x.Type.Contains("role")).Count() == 1 && 
                _user.IsInRole("Bereider"))
            {
                return bestellingen;
            }
            if (_user.Claims.Where(x => x.Type.Contains("role")).Count() == 1 &&
                _user.IsInRole("Klant") &&
                _user.Identity.Name != bestelling[0].KlantId.ToString())
            {
                throw new Exception("401");
            }
            else
            {
                return bestellingen;
            }
        }


        public async Task<GetBestellingenModel> PostBestelling(PostBestellingModel postBestellingModel)
        {
            List<StandaardPizza> standaardPizzas = new List<StandaardPizza>();

            foreach (var item in postBestellingModel.StandaardPizzas)
            {
                standaardPizzas.Add(await _context.StandaardPizzas.Where(x => x.Id.Equals(item)).FirstOrDefaultAsync());
            }


            EntityEntry<Bestelling> result = await _context.Bestellingen.AddAsync(new Bestelling
            {
                KlantId = postBestellingModel.KlantId,
                TijdstipBestelling = postBestellingModel.TijdstipBestelling
            });

            for (int i = 0; i < standaardPizzas.Count(); i++)
            {
                await _context.Pizzas.AddAsync(new Pizza
                {
                    Aantal = (int)postBestellingModel.StandaardPizzas.Select(x => x.Aantal).ToArray().GetValue(i),
                    StandaardPizzaId = (Guid)postBestellingModel.StandaardPizzas.Select(x => x.Id).ToArray().GetValue(i),
                    Bestelling = result.Entity
                });
            }

            await _context.SaveChangesAsync();
            return await GetBestellingen(result.Entity.Id);
        }


        public async Task PutBestelling(Guid id, PutBestellingModel putBestellingModel)
        {
            Bestelling bestelling = await _context.Bestellingen.FirstOrDefaultAsync(x => x.Id == id);
            if (bestelling == null)
            {
                throw new Exception("bestelling niet gevonden");
            }
            bestelling.BereiderId = putBestellingModel.BereiderId;
            bestelling.VoertuigId = putBestellingModel.VoertuigId;
            bestelling.VervoerderId = putBestellingModel.VervoerdeId;
            bestelling.TijdstipLevering = putBestellingModel.TijdstipLevering;

            await _context.SaveChangesAsync();
        }


        public async Task DeleteBestelling(Guid id)
        {
            Bestelling bestelling = await _context.Bestellingen.FirstOrDefaultAsync(x => x.Id == id);
            if (bestelling == null)
            {
                throw new Exception("bestelling niet gevonden");
            }

            _context.Bestellingen.Remove(bestelling);
            await _context.SaveChangesAsync();
        }
    }
}
