﻿using pizzadelivery.models.Users;
using System.Threading.Tasks;

namespace pizzadelivery.api.Repositories
{
    public interface IUserRepository
    {
        Task<PostAuthenticateResponseModel> Authenticate(PostAuthenticateRequestModel postAuthenticateRequestModel, string ipAddress);
        Task<PostAuthenticateResponseModel> RefreshToken(string token, string ipAddress);
        Task RevokeToken(string token, string ipAddress);
    }
}