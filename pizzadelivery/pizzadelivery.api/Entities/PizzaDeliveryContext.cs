﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Entities
{
    public class PizzaDeliveryContext : IdentityDbContext<
        User,
        Role,
        Guid,
        IdentityUserClaim<Guid>,
        UserRole,
        IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>,
        IdentityUserToken<Guid>
        >
    {
        public DbSet<Adres> Adressen { get; set; }
        public DbSet<Bestelling> Bestellingen { get; set; }
        public DbSet<Pizza> Pizzas { get; set; }
        public DbSet<StandaardPizza> StandaardPizzas { get; set; }
        public DbSet<Voertuig> Voertuigen { get; set; }
        public DbSet<PizzaTopping> PizzaToppings { get; set; }
        public DbSet<Topping> Toppings { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public PizzaDeliveryContext(DbContextOptions<PizzaDeliveryContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            if (builder == null) { throw new ArgumentNullException(nameof(builder)); }
            
            builder.Entity<Role>(b =>
            {
                b.HasMany(e => e.UserRoles)
                .WithOne(e => e.Role)
                .HasForeignKey(e => e.RoleId)
                .IsRequired();

            });

            builder.Entity<User>(b =>
            {
                b.HasMany(e => e.UserRoles)
                .WithOne(e => e.User)
                .HasForeignKey(e => e.UserId)
                .IsRequired();
            });

            builder.Entity<StandaardPizza>(b =>
            {
                b.HasMany(e => e.Pizzas)
                .WithOne(e => e.StandaardPizza)
                .HasForeignKey(e => e.StandaardPizzaId)
                .IsRequired();
            });

            builder.Entity<Topping>(b =>
            {
                b.HasMany(e => e.PizzaToppings)
                .WithOne(e => e.Topping)
                .HasForeignKey(e => e.ToppingId)
                .IsRequired();
            });
            builder.Entity<Voertuig>(b =>
            {
                b.HasMany(e => e.Bestellingen)
                .WithOne(e => e.Voertuig)
                .HasForeignKey(e => e.VoertuigId)
                .IsRequired();
            });
            
            builder.Entity<Adres>(b =>
            {
                b.HasOne(e => e.User)
                .WithMany(e => e.Adressen)
                .HasForeignKey(e => e.UserId)
                .IsRequired();
            });
            //tt
            builder.Entity<RefreshToken>(x =>
            {
                x.HasOne(x => x.User)
                .WithMany(x => x.RefreshTokens)
                .HasForeignKey(x => x.UserId)
                .IsRequired();
            });

            builder.Entity<Bestelling>(b =>
            {
                b.HasOne(e => e.Klant)
                 .WithMany()
                 .HasForeignKey(e => e.KlantId)
                 .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(e => e.Bereider)
               .WithMany()
               .HasForeignKey(e => e.BereiderId)
               .IsRequired(false)
               .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(e => e.Vervoerder)
               .WithMany()
               .HasForeignKey(e => e.VervoerderId)
               .IsRequired(false)
               .OnDelete(DeleteBehavior.Restrict);

                b.HasOne(e => e.Voertuig)
               .WithMany(e => e.Bestellingen)
               .HasForeignKey(e => e.VoertuigId)
               .IsRequired(false)
               .OnDelete(DeleteBehavior.Restrict);

                b.HasMany(e => e.Pizzas)
                .WithOne(e => e.Bestelling)
                .HasForeignKey(e => e.BestellingId)
                .IsRequired();
            });

            builder.Entity<Pizza>(b =>
            {
                b.HasOne(e => e.Bestelling)
                .WithMany(e => e.Pizzas)
                .HasForeignKey(e => e.BestellingId)
                .IsRequired();

                b.HasOne(e => e.StandaardPizza)
               .WithMany(e => e.Pizzas)
               .HasForeignKey(e => e.StandaardPizzaId)
               .IsRequired()
               .OnDelete(DeleteBehavior.Restrict);

                b.HasMany(e => e.PizzaToppings)
                .WithOne(e => e.Pizza)
                .HasForeignKey(e => e.PizzaId)
                .IsRequired();
            });

            builder.Entity<PizzaTopping>(b =>
            {
                b.HasOne(e => e.Pizza)
                .WithMany(e => e.PizzaToppings)
                .HasForeignKey(e => e.PizzaId)
                .IsRequired();

                b.HasOne(e => e.Topping)
               .WithMany(e => e.PizzaToppings)
               .HasForeignKey(e => e.ToppingId)
               .IsRequired();
            });

        }
    }
}
