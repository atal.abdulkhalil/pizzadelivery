﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Entities
{
    public class Voertuig
    {
        public Guid Id { get; set; }

        [Required]
        [EnumDataType(typeof(VoertuigType))]
        public VoertuigType Type { get; set; }
        
        [Required]
        [StringLength(10)]
        [RegularExpression(@"([A-Z]{3}-[0-9]{3})|(^\1-[A-Z]{3}-[0-9]{3}$)")]
        public string Nummerplaat { get; set; }

        public ICollection<Bestelling> Bestellingen { get; set; }
    }
}
