﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Entities
{
    public class Pizza
    {
        public Guid Id { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]+$")]
        public int Aantal { get; set; }

        public Guid BestellingId { get; set; }
        public Bestelling Bestelling { get; set; }

        public Guid StandaardPizzaId { get; set; }
        public StandaardPizza StandaardPizza { get; set; }

        public string Formaat { get; set; }

        public ICollection<PizzaTopping> PizzaToppings { get; set; }
    }
}
