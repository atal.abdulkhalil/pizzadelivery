﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Entities
{
    public class User : IdentityUser<Guid>
    {
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Voornaam { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Familienaam { get; set; }

        public ICollection<Adres> Adressen { get; set; }

        public ICollection<Bestelling> Bestellingen { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }

        public ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
