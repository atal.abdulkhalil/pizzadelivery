﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Entities
{
    public class Bestelling
    {
        public Guid Id { get; set; }

        public Guid KlantId { get; set; }
        public User Klant { get; set; }

        public Guid? BereiderId { get; set; }
        public User Bereider { get; set; }

        public Guid? VervoerderId { get; set; }
        public User Vervoerder { get; set; }

        public Guid? VoertuigId { get; set; }
        public Voertuig Voertuig { get; set; }

        [DataType(DataType.Date)]
        public DateTime? TijdstipBestelling { get; set; }

        [DataType(DataType.Date)]
        public DateTime? TijdstipLevering { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }
    }
}
