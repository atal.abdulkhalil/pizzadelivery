﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Entities
{
    public class Adres
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 6)]
        public string Straat { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]+$")]
        [StringLength(10)]
        public string Huisnummer { get; set; }

        [Required]
        [RegularExpression(@"^[0123]$")]
        public int Postcode { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Gemeente { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
