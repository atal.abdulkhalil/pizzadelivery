﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Entities
{
    public class PizzaTopping
    {
        public Guid Id { get; set; }

        public Guid PizzaId { get; set; }
        public Pizza Pizza { get; set; }

        public Guid ToppingId { get; set; }
        public Topping  Topping { get; set; }

        [Required]
        [RegularExpression(@"^[0-1]{1}$")]
        public bool MetOfZonder { get; set; }

    }
}
