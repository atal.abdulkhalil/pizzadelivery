﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Entities
{
    public class Topping
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Naam { get; set; }

        [Required]
        [RegularExpression(@"(\d+)|(\d+\,\d{1,2})")]
        [System.ComponentModel.DataAnnotations.Schema.Column(TypeName = "decimal(18, 2")]
        public decimal Prijs { get; set; }

        public ICollection<PizzaTopping> PizzaToppings { get; set; }
    }
}
