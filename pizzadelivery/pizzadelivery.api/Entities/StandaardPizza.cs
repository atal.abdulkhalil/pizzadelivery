﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Entities
{
    public class StandaardPizza
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Naam { get; set; }

        [Required]
        [RegularExpression(@"(\d+)|(\d+\,\d{1,2})")]
        [Column(TypeName = "decimal(18, 2")]
        public decimal PrijsKlein { get; set; }

        [Required]
        [RegularExpression(@"(\d+)|(\d+\,\d{1,2})")]
        [Column(TypeName = "decimal(18, 2")]
        public decimal PrijsMedium { get; set; }

        [Required]
        [RegularExpression(@"(\d+)|(\d+\,\d{1,2})")]
        [Column(TypeName = "decimal(18, 2")]
        public decimal PrijsGroot { get; set; }

        [Required]
        [StringLength(255)]
        public string Beschrijving { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }
    }
}
