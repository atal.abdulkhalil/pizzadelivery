﻿using Microsoft.Extensions.DependencyInjection;
using pizzadelivery.api.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Services
{
    public static class PizzadeliveryServiceCollectionExtensions
    {
        public static IServiceCollection AddPizzadelivery(this IServiceCollection service)
        {
            service.AddScoped<IBestellingenRepository, BestellingenRepository>();
            service.AddScoped<IUserRepository, UserRepository>();
            return service;
        }
    }
}
