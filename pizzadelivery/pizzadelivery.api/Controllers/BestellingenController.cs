﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pizzadelivery.api.Repositories;
using pizzadelivery.models.Bestellingen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class BestellingenController : ControllerBase
    {
        private readonly IBestellingenRepository _bestellingRepository;
        public BestellingenController(IBestellingenRepository bestellingRepository)
        {
            _bestellingRepository = bestellingRepository;
        }

      
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Bereider, Klant")]
        public async Task<ActionResult<GetBestellingenModel>> GetBestellingen()
        {
            return await _bestellingRepository.GetBestellingen();
        }


        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Bereider, Klant")]
        public async Task<ActionResult<GetBestellingenModel>> GetBestellingen(string id)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid bestellingId))
                {
                    throw new Exception("400");
                }
                GetBestellingenModel bestellingenModel = await _bestellingRepository.GetBestellingen(bestellingId);
                if (bestellingenModel.Bestelling.Count() == 0)
                {
                    throw new Exception("404");
                }
                return bestellingenModel;
                
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("400"))
                {
                    return BadRequest("ongeldig id");
                }
                if (ex.Message.Equals("404"))
                {
                    return NotFound("Bestelling niet gevonden");
                }
                else
                {
                    return NoContent();
                }
            }
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Authorize(Roles = "Klant")]
        public async Task<ActionResult<PostBestellingModel>> PostBestelling(PostBestellingModel postBestellingModel)
        {
            try
            {
                await _bestellingRepository.PostBestelling(postBestellingModel);
                return StatusCode(201);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Authorize(Roles = "Bereider")]
        public async Task<ActionResult> PutBestelling(string id, PutBestellingModel putBestellingModel)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid bestellingId))
                {
                    throw new Exception("400");
                }
                await _bestellingRepository.PutBestelling(bestellingId, putBestellingModel);
               
                return NoContent();

            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("400"))
                {
                    return BadRequest("Ongeldig id");
                }
                else
                {
                    return NotFound("Bestelling niet gevonden");
                }
            }

        }


        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Authorize(Roles = "Bereider")]
        public async Task<ActionResult> DeleteBestelling(string id)
        {
            try
            {
                if (!Guid.TryParse(id, out Guid bestellingId))
                {
                    throw new Exception("400");
                }
                await _bestellingRepository.DeleteBestelling(bestellingId);
                return NoContent();

            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("400"))
                {
                    return BadRequest("Ongeldig id");
                }
                else
                {
                    return NotFound("Bestelling niet gevonden");
                }
            }
        }



    }
    
}
