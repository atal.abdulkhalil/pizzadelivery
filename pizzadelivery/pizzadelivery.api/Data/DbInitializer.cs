﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using pizzadelivery.api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pizzadelivery.api.Data
{
    public class DbInitializer
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {

            PizzaDeliveryContext _context = serviceProvider.GetRequiredService<PizzaDeliveryContext>();
            UserManager<User> _userManager = serviceProvider.GetRequiredService<UserManager<User>>();
            RoleManager<Role> _rolManager = serviceProvider.GetRequiredService<RoleManager<Role>>();

            if (_context.Roles.Any())
            {
                return;
            }

            // roles aanmaken
            _ = _rolManager.CreateAsync(new Role { Name = "Klant", Omschrijving = "bestelt pizza en kan de info bekijken" }).Result;
            _ = _rolManager.CreateAsync(new Role { Name = "Bereider", Omschrijving = "kan de keuze van pizza bekijken." }).Result;
            _ = _rolManager.CreateAsync(new Role { Name = "Vervoerder", Omschrijving = "kan de adres van de klant en bestelling info bekijken." }).Result;

            // users aanmaken
            User Atal = new User
            {
                Voornaam = "Atal",
                Familienaam = "Abdulkhali",
                Email = "atal.ab@gmail.com",
                UserName = "atalab"
            };
            _ = _userManager.CreateAsync(Atal, "Azerty123?").Result;
            _ = _userManager.AddToRolesAsync(Atal, new List<string> { "Klant" }).Result;

            User Jarno = new User
            {
                Voornaam = "jarno",
                Familienaam = "Vermeire",
                Email = "Jarno@gmail.com",
                UserName = "JarnoV"
            };
            _ = _userManager.CreateAsync(Jarno, "Azerty123?").Result;
            _ = _userManager.AddToRolesAsync(Jarno, new List<string> { "Klant" }).Result;


            User bereiderWouter = new User
            {
                Voornaam = "Wouter",
                Familienaam = "Metsu",
                Email = "Wouter@gmail.com",
                UserName = "WouterM"
            };
            _ = _userManager.CreateAsync(bereiderWouter, "Azerty123?").Result;
            _ = _userManager.AddToRolesAsync(bereiderWouter, new List<string> { "Bereider" }).Result;

            User vervoerderThomas = new User
            {
                Voornaam = "Thomas",
                Familienaam = "Pauls",
                Email = "Thomas@gmail.com",
                UserName = "ThomasP"
            };
            _ = _userManager.CreateAsync(vervoerderThomas, "Azerty123?").Result;
            _ = _userManager.AddToRolesAsync(vervoerderThomas, new List<string> { "Vervoerder" }).Result;

            // adressen aanmaken
            Adres adresAtal = new Adres
            {
                UserId = Atal.Id,
                User = Atal,
                Straat = "lekestraat",
                Huisnummer = "85",
                Postcode = 9900,
                Gemeente = "Eeklo",
            };
            _context.Adressen.Add(adresAtal);

            Adres adresJarno = new Adres
            {
                UserId = Jarno.Id,
                User = Jarno,
                Straat = "stationstraat",
                Huisnummer = "55",
                Postcode = 9900,
                Gemeente = "Eeklo",
            };
            _context.Adressen.Add(adresJarno);

            // voertuigen aanmaken
            Voertuig BMW = new Voertuig
            {
                Type = VoertuigType.Auto,
                Nummerplaat = "1-CDC-120"
            };
            _context.Voertuigen.Add(BMW);

            Voertuig Audi = new Voertuig
            {
                Type = VoertuigType.Auto,
                Nummerplaat = "1-DVG-485"
            };
            _context.Voertuigen.Add(Audi);

            // bestellingen aanmaken
            Bestelling bestellingAtal = new Bestelling
            {
                TijdstipBestelling = new DateTime(2021, 01, 15, 17, 25, 45),
                TijdstipLevering = new DateTime(2021, 01, 15, 18, 00, 00),
                Klant = Atal,
                Bereider = bereiderWouter,
                Vervoerder = vervoerderThomas,
                Voertuig = BMW,

            };
            _context.Bestellingen.Add(bestellingAtal);

            Bestelling bestellingJarno = new Bestelling
            {
                TijdstipBestelling = new DateTime(2021, 01, 15, 17, 33, 44),
                TijdstipLevering = new DateTime(2020, 01, 15, 18, 05, 00),
                Klant = Jarno,
                Bereider = bereiderWouter,
                Vervoerder = vervoerderThomas,
                Voertuig = Audi,

            };
            _context.Bestellingen.Add(bestellingJarno);

            // standaardPizzas aanmaken
            StandaardPizza pepperoni = new StandaardPizza
            {
                Naam = "pepperoni",
                PrijsKlein = 09.90M,
                PrijsMedium = 10.90M,
                PrijsGroot = 12.90M,
                Beschrijving = "dubbele portie pepperoni, paprika, jalapenos"
            };
            _context.StandaardPizzas.Add(pepperoni);

            StandaardPizza fullOption = new StandaardPizza
            {
                Naam = "Full Option",
                PrijsKlein = 09.95M,
                PrijsMedium = 12.95M,
                PrijsGroot = 15.95M,
                Beschrijving = "salami, paprika, ui, olijven, kip, ananas, maïs, gehakt, extra kaas"
            };
            _context.StandaardPizzas.Add(fullOption);


            // pizzas aanmaken
            Pizza pizzaJarno = new Pizza
            {
                Aantal = 6,
                Formaat = "medium",
                Bestelling = bestellingJarno,
                StandaardPizza = fullOption
            };
            _context.Pizzas.Add(pizzaJarno);

            Pizza pizzaJarno2 = new Pizza
            {
                Aantal = 1,
                Formaat = "groot",
                Bestelling = bestellingJarno,
                StandaardPizza = fullOption
            };
            _context.Pizzas.Add(pizzaJarno2);

            Pizza pizzaAtal = new Pizza
            {
                Aantal = 3,
                Formaat = "medium",
                Bestelling = bestellingAtal,
                StandaardPizza = pepperoni
            };
            _context.Pizzas.Add(pizzaAtal);

            // toppings aanmaken
            Topping kaas = new Topping
            {
                Naam = "fetakaas",
                Prijs = 1.00M
            };
            _context.Toppings.Add(kaas);


            // pizzaToppings aanmaken
            PizzaTopping toppingAtal = new PizzaTopping
            {
                MetOfZonder = true,
                Pizza = pizzaAtal,
                Topping = kaas
            };
            _context.PizzaToppings.Add(toppingAtal);

            _context.SaveChanges();
        }
    }
}
