﻿using pizzadelivery.models.StandaardPizza;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace pizzadelivery.models.Bestellingen
{
    public class PostBestellingModel
    {
        public Guid KlantId { get; set; }

        [DataType(DataType.Date)]
        public DateTime TijdstipBestelling { get; set; }

        public ICollection<PostStandaardPizzaModel> StandaardPizzas { get; set; }
    }
}
