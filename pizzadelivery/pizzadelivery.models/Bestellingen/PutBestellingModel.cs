﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pizzadelivery.models.Bestellingen
{
    public class PutBestellingModel
    {
        public Guid BereiderId { get; set; }
        public Guid VervoerdeId { get; set; }
        public Guid VoertuigId { get; set; }
        public DateTime TijdstipLevering { get; set; }
    }
}
