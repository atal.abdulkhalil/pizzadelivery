﻿using pizzadelivery.models.StandaardPizza;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace pizzadelivery.models.Bestellingen
{
    public class GetBestellingModel
    {
        public Guid Id { get; set; }
        public Guid KlantId { get; set; }
        public string Klant { get; set; }
        public Guid BereiderId { get; set; }
        public string Bereider { get; set; }
        public Guid VervoerderId { get; set; }
        public string Vervoerder { get; set; }
        public Guid VoertuigId { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime TijdstipBestelling { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime TijdstipLevering { get; set; }

        public ICollection<GetStandaardPizzaModel> Pizzas { get; set; }

        public decimal TotaalPrijs { get; set; }
    }
}
