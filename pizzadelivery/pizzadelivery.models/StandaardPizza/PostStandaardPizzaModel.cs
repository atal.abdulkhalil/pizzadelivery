﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pizzadelivery.models.StandaardPizza
{
    public class PostStandaardPizzaModel
    {
        public Guid Id { get; set; }
        public int Aantal { get; set; }
    }
}
