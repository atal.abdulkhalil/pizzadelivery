﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pizzadelivery.models.StandaardPizza
{
    public class GetStandaardPizzaModel
    {
        public string Naam { get; set; }
        public int Aantal { get; set; }
        public string Formaat { get; set; }
        public decimal EenheidPrijs { get; set; }
        public decimal TotaalPrijs { get; set; }


    }
}
