﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pizzadelivery.models.Users
{
    public class PostRevokeTokenRequestModel
    {
        public string Token { get; set; }
    }
}
